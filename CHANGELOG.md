# Changelog

All notable changes to this project will be documented in this file. Dates are displayed in UTC.

## [v1.2.0](https://gitlab.com/fberat/mass-prebuild/-/releases/v1.2.0) - 28 September 2023

### Release Highlights

This release brings support for new backends.

There are 2 added, the `mock` backend and the `remote_mock` backend.
As the name states, the first one uses mock in order to build the packages on
the local host, while the later does the same but on a remote host (for which a
valid address is given).
The remote must be properly set up, which means the ssh connection can be
established without interaction and MPB is available on this remote with a
version that does support mock builds.

This also implies the support for a new `daemonize` option, so that we can
execute MPB in the background for better mock builds monitoring.

### Changes summary

- Display build location during the build (Closes #87)
- Implement mock and remote-mock back-ends (Closes #71)
- Deadlock on clean-db (Closes #85)

### Detailed changes

#### New Features

- Rework logging mechanism @Frédéric Bérat [`9c8fd5a`](https://gitlab.com/fedora/packager-tools/mass-prebuild/commit/9c8fd5aada02ad11f966e9db3038d36073e169d5)
- Implement mock back-end @Frédéric Bérat [`fe083c4`](https://gitlab.com/fedora/packager-tools/mass-prebuild/commit/fe083c447d1c48b2d451c99ac3fc7aec19fad2fd)
- Add remote back-end @Frédéric Bérat [`e334e85`](https://gitlab.com/fedora/packager-tools/mass-prebuild/commit/e334e85b54ea4930e29afb123d381fa31c6d22e4)
- set build location for {remote-,}mock @Frédéric Bérat [`673c27b`](https://gitlab.com/fedora/packager-tools/mass-prebuild/commit/673c27b583df7b6a35d217a8e9c16242f5d9a36f)
- Add build location to user build information @Frédéric Bérat [`c5a47dc`](https://gitlab.com/fedora/packager-tools/mass-prebuild/commit/c5a47dc3b87ecb7dad0b525141a166f1beb19b1b)

#### Fixes

- Deadlock on clean-db @Frédéric Bérat [`a628b70`](https://gitlab.com/fedora/packager-tools/mass-prebuild/commit/a628b708d58edbaa82093125628b387a3ee7359c)
- Stabilize dummy info @Frédéric Bérat [`3b834b6`](https://gitlab.com/fedora/packager-tools/mass-prebuild/commit/3b834b694d2f4833c7fae00a608bdf260a1d8042)
- Ensure committish is a string before checking first character @Frédéric Bérat [`c8c17e1`](https://gitlab.com/fedora/packager-tools/mass-prebuild/commit/c8c17e182ba3f2cc5e5c5cd89987e1e7b186d745)
- Fix version from git @Frédéric Bérat [`835b11e`](https://gitlab.com/fedora/packager-tools/mass-prebuild/commit/835b11e6b3b935d1c94013022d1d96ef3b2361b6)

#### Changes to Test Assests

- Add tests for mock back-end @Frederic Berat [`6114f7e`](https://gitlab.com/fedora/packager-tools/mass-prebuild/commit/6114f7e7671472669658fbd450b26cee13eacb2a)
- Speed-up cli tests @Frédéric Bérat [`eaf677d`](https://gitlab.com/fedora/packager-tools/mass-prebuild/commit/eaf677d7e8a6ddfbc473508a08f5ec77ab320b8a)

#### General Changes

- Update COPYING @Carlos O'Donell (Red Hat) [`91579c4`](https://gitlab.com/fedora/packager-tools/mass-prebuild/commit/91579c41ae4b40d26ae7ed370666138ef0729cbf)

## [v1.1.0](https://gitlab.com/fberat/mass-prebuild/-/releases/v1.1.0) - 24 July 2023

### Release Highlights

This release mainly brings bug-fixes.

### Changes summary

- Drop hard requirement on default repository configuration (Closes #80)
- Add basic tests for file source type (Closes #78)
- Add build location in report (Closes #77)
- Remove duplicates from package groups (Closes #83)
- Add control-failed to default log collection (Closes #81)
- set checker default values (Closes #79)
- Store full path in src (Closes #76)

### Detailed changes

#### New Features

- Add support for copr.ownername key in config file @Tom Stellard [`8ed1a5c`](https://gitlab.com/fedora/packager-tools/mass-prebuild/commit/8ed1a5cfada930809758010d6d6f208135e143f3)
- Drop hard requirement on default repository configuration @Frederic Berat [`4a2a1da`](https://gitlab.com/fedora/packager-tools/mass-prebuild/commit/4a2a1da9f8fba930a702c9f8903d26bb1e0359f3)
- Add build location in report @Frederic Berat [`033c0a6`](https://gitlab.com/fedora/packager-tools/mass-prebuild/commit/033c0a60b2f85bfe94140053f165c43b460bdeba)
- Add control-failed to default log collection @Frederic Berat [`40fcf3d`](https://gitlab.com/fedora/packager-tools/mass-prebuild/commit/40fcf3d371c9c68e54b0cecb645f0acc67fcc22d)
- Add support for copr.ownername key in config file () @Frédéric Bérat [`59ea02d`](https://gitlab.com/fedora/packager-tools/mass-prebuild/commit/59ea02d836739327202792df85ea096e3de722df)

#### Fixes

- set checker default values @Frédéric Bérat [`c8915ea`](https://gitlab.com/fedora/packager-tools/mass-prebuild/commit/c8915ea5e3ddccbd5a4bd5e1e9ae31ec9eafbf82)
- Remove duplicates from package groups @Frederic Berat [`8e594cf`](https://gitlab.com/fedora/packager-tools/mass-prebuild/commit/8e594cfcb985571f82dc4ee347587ffc0a700eba)
- Wake-up checker after watch walk through () @Frédéric Bérat [`9cf896d`](https://gitlab.com/fedora/packager-tools/mass-prebuild/commit/9cf896d1cf49497a7b4f674230eabc1da4dd6cd9)
- Wake-up checker after watch walk through @Frédéric Bérat [`eb05fb7`](https://gitlab.com/fedora/packager-tools/mass-prebuild/commit/eb05fb7464e50f045f7a5a8bfb97e6826599e505)
- Store full path in src @Frédéric Bérat [`e56aaf9`](https://gitlab.com/fedora/packager-tools/mass-prebuild/commit/e56aaf9c80a1e8bb1afe099280dc4ed261bba25e)

#### Changes to Test Assests

- Add basic tests for file source type @Frederic Berat [`d26abe6`](https://gitlab.com/fedora/packager-tools/mass-prebuild/commit/d26abe68b02590be68ea296261592c70cc77b7d4)

#### General Changes

- ci: Rebase pre-commit patch @Frédéric Bérat [`309c497`](https://gitlab.com/fedora/packager-tools/mass-prebuild/commit/309c4979c216b5dfdb21c5896786d8f0c86dfb58)
- ci: Update pre-commit config () @Frédéric Bérat [`1882fd4`](https://gitlab.com/fedora/packager-tools/mass-prebuild/commit/1882fd4f13e69eafc14a9207e192000ccd2d670b)
- ci: Update pre-commit config @Frederic Berat [`03f4cdd`](https://gitlab.com/fedora/packager-tools/mass-prebuild/commit/03f4cdd64a37b5b48aafa96ecfbaa833c30b9e09)

## [v1.0.0](https://gitlab.com/fberat/mass-prebuild/-/releases/v1.0.0) - 3 March 2023

### Release Highlights

The following new features are added:

  - man-pages may now be generated, from asciidoc files
  - A report utility has been added, to help the user to generate reports to be
    provided to its community
  - In bash/tsh environments auto-completion of the commands may now be
    supported through the python3-argcomplete package

This release marks a milestone in the mass pre-builder.
The support for COPR back-end is considered to be completed, as are the major
features that were originally required.

### Changes summary

- Add support for relative path in config (Closes #74)
- Mark cleaned build in --list (Closes #68)
- Add report utility (Closes #65)
- Add man pages (Closes #69)

### Detailed changes

#### New Features

- Add support for relative path in config @Frédéric Bérat [`1ffa580`](https://gitlab.com/fedora/packager-tools/mass-prebuild/commit/1ffa5808ee7fd60497cfc1e8d14c4cb8d5d8aea2)
- Add report utility @Frederic Berat [`15c2521`](https://gitlab.com/fedora/packager-tools/mass-prebuild/commit/15c252146f0bedd6f4fa3ec9a5981a5944dc4acb)
- Add support for git versioning in pip install @Frédéric Bérat [`7c4818f`](https://gitlab.com/fedora/packager-tools/mass-prebuild/commit/7c4818f3269f8410c4f198bdd8f93bac52db4bef)
- Add autocomplete support @Frédéric Bérat [`7d18c5d`](https://gitlab.com/fedora/packager-tools/mass-prebuild/commit/7d18c5d9ebc009059fa736179386e2ffb865f8d7)
- Mark cleaned build in --list @Frédéric Bérat [`0fec3ee`](https://gitlab.com/fedora/packager-tools/mass-prebuild/commit/0fec3ee5ddeaf49770397d399639622ee3f196c9)
- whatrequires: Enable verbosity while loading configuration @Frederic Berat [`562f7d3`](https://gitlab.com/fedora/packager-tools/mass-prebuild/commit/562f7d3739c471afb0f4759648f7ec346ccea17e)

#### Fixes

- Rework database locking @Frédéric Bérat [`19b4f5f`](https://gitlab.com/fedora/packager-tools/mass-prebuild/commit/19b4f5f27880f4f4ebec02c8f260d2e6e5125ba2)
- Ensure checker build ID isn't corrupted @Frédéric Bérat [`93964df`](https://gitlab.com/fedora/packager-tools/mass-prebuild/commit/93964dfb3cf1fa3d1c3d90e4cf2b6725238604fe)
- copr: New exception phrasing @Frédéric Bérat [`d09e82d`](https://gitlab.com/fedora/packager-tools/mass-prebuild/commit/d09e82d36906417f11887fcdf04bb1d28528e0f4)

#### Documentation Changes

- Add man pages @Frédéric Bérat [`ce1ebcd`](https://gitlab.com/fedora/packager-tools/mass-prebuild/commit/ce1ebcdb5f95663d3720b0e1ac98678e0ce5b8e9)
- Add version line in mpb-report example @Frédéric Bérat [`a3f7dd9`](https://gitlab.com/fedora/packager-tools/mass-prebuild/commit/a3f7dd9fa92fa0ed1e48589cd5b2d58d38daf30e)
- Fix dist git address in README @Frédéric Bérat [`fde211d`](https://gitlab.com/fedora/packager-tools/mass-prebuild/commit/fde211d21a5c63660e4b51ef866e6ba7fc5fbf8f)

#### Refactoring and Updates

- Improve support for rhel configurations @Frédéric Bérat [`4411710`](https://gitlab.com/fedora/packager-tools/mass-prebuild/commit/4411710d0ce0e72b74a7ee8edbb44d70f4c3c36a)
- Don't use python 3.6 incompatible constructs @Frédéric Bérat [`d176692`](https://gitlab.com/fedora/packager-tools/mass-prebuild/commit/d17669264e6bccc98d2ea1c04e6cd133b75de28e)

#### Changes to Test Assests

- Adapt fake repo configuration @Frédéric Bérat [`301eacd`](https://gitlab.com/fedora/packager-tools/mass-prebuild/commit/301eacd353bf9e05fd7f10acf7c4f4d77f125715)
- Improve debug CI failure @Frédéric Bérat [`05172fc`](https://gitlab.com/fedora/packager-tools/mass-prebuild/commit/05172fc8484c84d91be2086e2f99e1ab1602c06d)
- Handle login failure in COPR @Frédéric Bérat [`13252fd`](https://gitlab.com/fedora/packager-tools/mass-prebuild/commit/13252fd30baa6a585b8fb0af096c9ddb87023436)

#### Tidying of Code eg Whitespace

- Properly align build list @Frédéric Bérat [`83f32f0`](https://gitlab.com/fedora/packager-tools/mass-prebuild/commit/83f32f03a9172cd6ed146ef91957a4a9869a0e59)

#### General Changes

- Various fixes @Frédéric Bérat [`013c1e2`](https://gitlab.com/fedora/packager-tools/mass-prebuild/commit/013c1e2a3440c11e7875e66e29fdc063ba1d15ef)
- ci: Fix gitlab pre-commit patch for root environment @Frédéric Bérat [`2d3c3d7`](https://gitlab.com/fedora/packager-tools/mass-prebuild/commit/2d3c3d7ff11c74547d06f3518595d2e2fdbe7d0e)
- Improve support for RHEL @Frédéric Bérat [`f738bd3`](https://gitlab.com/fedora/packager-tools/mass-prebuild/commit/f738bd340e7632da4fe1b7a0eb5615cf569c37a1)
- Post release fixes @Frédéric Bérat [`f70c2f6`](https://gitlab.com/fedora/packager-tools/mass-prebuild/commit/f70c2f6d9b89e9a68ba63c1f13cc7a1a212dff03)

## [v0.6.0](https://gitlab.com/fberat/mass-prebuild/-/releases/v0.6.0) - 9 February 2023

### Release Highlights

This release should help user to deal with complex dependency graphs, that were
not properly calculated by MPB:

  - The `retry` configuration can now be set to `dynamic`: packages priority is
    bumped on failure, and it will be rebuilt, unless there is no longer
    successful builds in lower priorities.
  - A `rebuild` argument is now available, so that failed build are rebuilt.
    User may provide a subset of package names, to limit the amount of package
    to be rebuilt.

The `cleanup` command has been replaced by `clean-db` to improve clarity. The
documentation has also been improved, typical MPB work-flows are described, and
more may be added on demand.

It is now possible to provided dedicated `COPR` configuration to the checker
build. This also implies that, as an example, if the user wants to provided a
dedicated external repository during the build, it should be specifically
indicated in the checker build. By default, external repositories that are
provided for the main build are not used in the checker build.

### Changes summary

- Indicate the amount of build that are not yet started (Closes #58)
- Ensure the default branch is set when needed (Closes #67)
- Clarify some options (Closes #59, #54, #64, and #55)
- copr: Introduce exponential Backoff and Jitter on reconnect (Closes #66)
- Support checker dedicated configurations (Closes #61)
- Fix build order calculation (Closes #62)
- Add new way to retry builds (Closes #52 and #51)
- Rework dependency calculation (Closes #56)
- Break down thread loops (Closes #50)
- Checker build never started (Closes #48)

### Detailed changes

#### New Features

- Dynamic retry on failure @Frédéric Bérat [`29143c5`](https://gitlab.com/fedora/packager-tools/mass-prebuild/commit/29143c59b089aa64ad8c86750b31d5efa06b474c)
- Add a package rebuild capability @Frédéric Bérat [`345bc3d`](https://gitlab.com/fedora/packager-tools/mass-prebuild/commit/345bc3df19142130f6b5ae0cf25e4ccbb0bd9a0e)
- Support checker dedicated configurations @Frédéric Bérat [`9cd7039`](https://gitlab.com/fedora/packager-tools/mass-prebuild/commit/9cd703931bf62e65cc4c6b2bd802a2c444a3375f)
- Indicate the amount of build that are not yet started @Frédéric Bérat [`fc04a1c`](https://gitlab.com/fedora/packager-tools/mass-prebuild/commit/fc04a1c972151e90951c152c2827852dac7c5fab)
- Add --no-priority argument to mpb-whatrequires @Frédéric Bérat [`ff7db77`](https://gitlab.com/fedora/packager-tools/mass-prebuild/commit/ff7db7757c5c049f1f7977031ad21596c2fadb53)

#### Fixes

- copr: Introduce exponential Backoff and Jitter on reconnect @Frédéric Bérat [`16468b7`](https://gitlab.com/fedora/packager-tools/mass-prebuild/commit/16468b78dfe3c95e37b828992ee00952c0a48bf8)
- rework reverse dependency calculation @Miro Hrončok [`db82ae1`](https://gitlab.com/fedora/packager-tools/mass-prebuild/commit/db82ae1722c040be43735e09a06e2827e1019105)
- Break down thread loops @Frédéric Bérat [`24899d2`](https://gitlab.com/fedora/packager-tools/mass-prebuild/commit/24899d2e9a2b46bbcf3bd97a19945b21aa80219a)
- Ensure the correct branch is set when no committish is provided @Frédéric Bérat [`ab5502b`](https://gitlab.com/fedora/packager-tools/mass-prebuild/commit/ab5502b1709360042b989eddcd7d37b3c9c50b24)
- Reorder worker startup @Frédéric Bérat [`2efa16d`](https://gitlab.com/fedora/packager-tools/mass-prebuild/commit/2efa16dc1d2d4e7d446f2d152ccae493da8c65f0)
- copr: Handle case where the user deleted a build @Frédéric Bérat [`9cf0f39`](https://gitlab.com/fedora/packager-tools/mass-prebuild/commit/9cf0f39a297d97024f904330b950fb11c3b75647)
- fix(copr_edit): Reset committish when automatic rebuild is enabled @Frédéric Bérat [`5672f78`](https://gitlab.com/fedora/packager-tools/mass-prebuild/commit/5672f78fbf294daec4407cfb7d6f8d7c7e029aa0)
- Checker build never started @Frédéric Bérat [`6b6e21b`](https://gitlab.com/fedora/packager-tools/mass-prebuild/commit/6b6e21b9b71c44f444507c3b424837daa5726b31)

#### Chores And Housekeeping

- Version update @Frédéric Bérat [`1893892`](https://gitlab.com/fedora/packager-tools/mass-prebuild/commit/1893892a797c9ab9e198e379e6bc7c03b1924fbd)
- Version update @Frédéric Bérat [`2b613b0`](https://gitlab.com/fedora/packager-tools/mass-prebuild/commit/2b613b0a9998882ac496e8f6d676a248bd9a08b2)
- Version update @Frédéric Bérat [`c4cf591`](https://gitlab.com/fedora/packager-tools/mass-prebuild/commit/c4cf59163272079f97b6ed9f73e79eea5474d27a)

#### Documentation Changes

- Add use case descriptions @Frédéric Bérat [`1da2e19`](https://gitlab.com/fedora/packager-tools/mass-prebuild/commit/1da2e191dcac5d8a48c094c46d22be0945eb0038)
- Increase information provided by the help @Frédéric Bérat [`72f53aa`](https://gitlab.com/fedora/packager-tools/mass-prebuild/commit/72f53aa79872ad8f762fd21542e1431d40031982)
- Add a note regarding priority calculation @Frédéric Bérat [`4a071f2`](https://gitlab.com/fedora/packager-tools/mass-prebuild/commit/4a071f22fa25df9eedd4a9bfece9b1a79cebaef6)
- Improve README information about mpb-failedconf @Frédéric Bérat [`b62639a`](https://gitlab.com/fedora/packager-tools/mass-prebuild/commit/b62639aef892ca81a4dca7b56572196cc7e8c227)
- Add a description to the name configuration option @Frédéric Bérat [`0fb77c9`](https://gitlab.com/fedora/packager-tools/mass-prebuild/commit/0fb77c93f6952b3e85d8e326e4b340645e05cc2b)
- Add examples to COPR specific configuration @Frédéric Bérat [`d7266bf`](https://gitlab.com/fedora/packager-tools/mass-prebuild/commit/d7266bffbf3197f5e924b32fe769d4e1b2b7dabc)

#### Refactoring and Updates

- Calculate build order on the fly @Frédéric Bérat [`421ddc4`](https://gitlab.com/fedora/packager-tools/mass-prebuild/commit/421ddc454f72302922ac3d3b0cc6ed260686827d)
- Rework priority calculation @Frédéric Bérat [`8b94729`](https://gitlab.com/fedora/packager-tools/mass-prebuild/commit/8b94729ae6559372e052a05187e7938b7f1b5b0f)
- Adapt get_last_build to get_reverse_deps changes @Frédéric Bérat [`4da899a`](https://gitlab.com/fedora/packager-tools/mass-prebuild/commit/4da899a85086784b4de506df080a12d311825bab)
- Walk through all package at least once in watch agent @Frédéric Bérat [`5caa362`](https://gitlab.com/fedora/packager-tools/mass-prebuild/commit/5caa362643e63617893ad95d05ff800d246602fc)
- Rename cleanup command to clean-db @Frédéric Bérat [`bdb2f4d`](https://gitlab.com/fedora/packager-tools/mass-prebuild/commit/bdb2f4dd22a2f43c454a9961d56423ac7a1ab72c)
- Save some memory at the end of revdeps calculation @Frédéric Bérat [`3082e76`](https://gitlab.com/fedora/packager-tools/mass-prebuild/commit/3082e76eaa53145878026064292d6de399c28969)
- Replace posix-ipc with filelocks @Frédéric Bérat [`f318e7f`](https://gitlab.com/fedora/packager-tools/mass-prebuild/commit/f318e7f408a5e9c85a94add4fb3455b0e078c5c3)
- Replace posix-ipc with filelocks () @Frédéric Bérat [`91f91e6`](https://gitlab.com/fedora/packager-tools/mass-prebuild/commit/91f91e686d0d096e8fa53d023867ad828eaba2dd)
- Don't got from list to set unnecessarily @Miro Hrončok [`50dd807`](https://gitlab.com/fedora/packager-tools/mass-prebuild/commit/50dd8074a72060e7d0fd710a758f6ba59c7088e9)

#### Changes to Test Assests

- Add dynamic retry test and adapt retry feature test to new logs @Frédéric Bérat [`3277178`](https://gitlab.com/fedora/packager-tools/mass-prebuild/commit/32771788cfaf9375141ebe3ccd1490e13b2a5081)
- Add a timeout to the tests @Frédéric Bérat [`2014edc`](https://gitlab.com/fedora/packager-tools/mass-prebuild/commit/2014edccd9a8a8bf42eebb1d4554365b755bffb3)
- Python upgrade to 3.11 @Frédéric Bérat [`5e368e3`](https://gitlab.com/fedora/packager-tools/mass-prebuild/commit/5e368e37941bb516bc688bea8f35b806e31101ec)
- Exclude checker config from default config test @Frédéric Bérat [`c3789f9`](https://gitlab.com/fedora/packager-tools/mass-prebuild/commit/c3789f96fe4ddc8ad8bf09245103826ea5330f5f)
- Adapt tests to new branch defaults @Frédéric Bérat [`1544919`](https://gitlab.com/fedora/packager-tools/mass-prebuild/commit/154491969350fbd4e070514174a7cd69925952cc)

## [v0.5.0](https://gitlab.com/fberat/mass-prebuild/-/releases/v0.5.0) - 14 November 2022

### Release Highlights

This release brings new tools for the user:

 - mpb-failedconf script allows to generate a configuration file which
   contains the package that have failed in a given build, which allows
   to have a short-cut to verify a fix.
 - mpb-copr-edit script allows the user to edit a copr project in order
   to massively enable the webhook-rebuild option. This can only be used
   if the original MPB build is done.

It is now possible to provide a full configuration to COPR project,
which may contain a dedicated description, specific YUM/DNF repositories
or even a different mock configuration.

And, as always, various improvement and bug fixes, such as:

 - Once and MPB build has been cleaned, the user may execute the 'mpb
   --cleanup' command which will remove the corresponding entries from the
   database.
 - Support for verbosity in the --list command, which may now show the
   builds that have been "cleaned" and the control builds
 - COPR connection cache is automatically cleared in case of connection
   failure

### Changes summary

- Create a tool that generates a configuration file containing the list of failed packages from a given MPB build (Closes #45)
- Customize copr projects (Closes #10)
- Prevent database lock exception (Closes #44)

### Detailed changes

#### New Features

- Add mpb_failedconf script @Frédéric Bérat [`b2d35b2`](https://gitlab.com/fedora/packager-tools/mass-prebuild/commit/b2d35b285bae4c26f780c0c5efd6829303aad731)
- Add a script dedicated to copr project edition @Frédéric Bérat [`135f535`](https://gitlab.com/fedora/packager-tools/mass-prebuild/commit/135f5351b35a6251f5c363cebc001940a3bd7fa7)
- copr: Add more configuration options for packages @Frédéric Bérat [`7199e58`](https://gitlab.com/fedora/packager-tools/mass-prebuild/commit/7199e58c30d9bb3e4be7020c128b4381a6c77aa8)
- copr: Allow deep configuration of the project @Frédéric Bérat [`9c71708`](https://gitlab.com/fedora/packager-tools/mass-prebuild/commit/9c717081b87f35819cb981ea941fc02b294c12d3)
- Add database cleanup command @Frédéric Bérat [`55e6128`](https://gitlab.com/fedora/packager-tools/mass-prebuild/commit/55e6128a12ab0d711197b186997fd461e39f7e73)
- Support verbosity along with list command @Frédéric Bérat [`c246ca7`](https://gitlab.com/fedora/packager-tools/mass-prebuild/commit/c246ca784534bc1ab087b08f7a0cd101fd8aec71)

#### Fixes

- Prevent database lock exception @Frédéric Bérat [`b1fbfb5`](https://gitlab.com/fedora/packager-tools/mass-prebuild/commit/b1fbfb5d8c7eb0818770797af9d9a799f209bf4c)
- failedconf: Handle case were some config entries don't exist @Frédéric Bérat [`979790d`](https://gitlab.com/fedora/packager-tools/mass-prebuild/commit/979790d6024e2dc480b7b3a716ee2dea4b58766b)
- copr: Try to connect on startup @Frédéric Bérat [`c3651f9`](https://gitlab.com/fedora/packager-tools/mass-prebuild/commit/c3651f97e10c3d8db1f5b305cd868138e8662680)
- copr: Reset monitor when new build is added @Frédéric Bérat [`49a0958`](https://gitlab.com/fedora/packager-tools/mass-prebuild/commit/49a0958178d0364bda622ba09103f2e3125e1347)
- Handle empty strings set for skipped arches @Frédéric Bérat [`831b9bf`](https://gitlab.com/fedora/packager-tools/mass-prebuild/commit/831b9bfb3bc621117946ded39e735893bd192e14)
- Don't overwrite stored data and dnf_conf @Frédéric Bérat [`a5eebe8`](https://gitlab.com/fedora/packager-tools/mass-prebuild/commit/a5eebe8570a6870c8ab5d4f040567ae69e0c3bd5)
- copr: Missing break in check loop @Frédéric Bérat [`d058904`](https://gitlab.com/fedora/packager-tools/mass-prebuild/commit/d0589040a8e5c4ea594f6aba9f7e97090144e33b)

#### Documentation Changes

- Update README for v0.5.0 @Frédéric Bérat [`f21c3f0`](https://gitlab.com/fedora/packager-tools/mass-prebuild/commit/f21c3f063a2ea358a699bfac464ea80ee32b35b3)
- Detail COPR specific configuration options @Frédéric Bérat [`67be295`](https://gitlab.com/fedora/packager-tools/mass-prebuild/commit/67be295be582dd2644944b3ea1a37a8ded8ede15)

#### Refactoring and Updates

- Reduce amount of branches and statements in main @Frédéric Bérat [`86e7fc4`](https://gitlab.com/fedora/packager-tools/mass-prebuild/commit/86e7fc4fb66f538b1b05d7e4b50662612fa0a46c)
- Externalize repo configurations @Frédéric Bérat [`322c5e0`](https://gitlab.com/fedora/packager-tools/mass-prebuild/commit/322c5e006550e6beaa62a7444f52c7ec6b1f294a)
- copr: Extract base configuration into a dedicated file @Frédéric Bérat [`719c88c`](https://gitlab.com/fedora/packager-tools/mass-prebuild/commit/719c88c54cf8040f15303f829313b76f9cd46925)
- copr: Process accepted chroots in a dedicated function @Frédéric Bérat [`f3b1583`](https://gitlab.com/fedora/packager-tools/mass-prebuild/commit/f3b15839da93f0757c7bf36716cbe955fadd7a24)
- Move additional script to utils subdirectory @Frédéric Bérat [`6349ce2`](https://gitlab.com/fedora/packager-tools/mass-prebuild/commit/6349ce2ea2d3ab77ffedf02453897a3d547113aa)
- Give more information to user regarding database migration @Frédéric Bérat [`7064ddc`](https://gitlab.com/fedora/packager-tools/mass-prebuild/commit/7064ddc18f34a92b322cd0791d67b8755c66f00c)

#### Changes to Test Assests

- Add concurrency test @Frédéric Bérat [`c7f3a3d`](https://gitlab.com/fedora/packager-tools/mass-prebuild/commit/c7f3a3dfd8ea1ec6db2793db1a96a0a562993489)
- Add date information @Frédéric Bérat [`dcc49dd`](https://gitlab.com/fedora/packager-tools/mass-prebuild/commit/dcc49ddee823112f819d02a741d558f56b3054f2)
- Make repo-conf standalone @Frédéric Bérat [`60fb7e3`](https://gitlab.com/fedora/packager-tools/mass-prebuild/commit/60fb7e3a5f50740dfe3842fa913613f156435a6f)
- Add mpb-failedconf and mpb-copr to version and help tests @Frédéric Bérat [`beae145`](https://gitlab.com/fedora/packager-tools/mass-prebuild/commit/beae14543cd52b12670f97b9922e6fff99ba629b)
- Silent kills @Frédéric Bérat [`aa7f5b2`](https://gitlab.com/fedora/packager-tools/mass-prebuild/commit/aa7f5b249bec925da0041fbe1fac7ef7a0caccd6)

#### General Changes

- ci: Don't use cache @Frédéric Bérat [`3d9cec7`](https://gitlab.com/fedora/packager-tools/mass-prebuild/commit/3d9cec7d80fd9db1aa189bf25f882d98214e8547)
- Extract built-in configuration into dedicated configuration files () @Frédéric Bérat [`d146180`](https://gitlab.com/fedora/packager-tools/mass-prebuild/commit/d14618092c66f604631ad000e3c51969e7d77a00)
- ci: Install configuration files @Frédéric Bérat [`00f06cf`](https://gitlab.com/fedora/packager-tools/mass-prebuild/commit/00f06cf90784311119a136ea290e4fe30a1a332f)
- Various improvements @Frédéric Bérat [`df90af6`](https://gitlab.com/fedora/packager-tools/mass-prebuild/commit/df90af65025d5187c225c6edf367c4d19dc1b6ed)

## [v0.4.0](https://gitlab.com/fberat/mass-prebuild/-/releases/v0.4.0) - 27 October 2022

### Release Highlights

This release brings major improvements to the UI, since it is now possible to:

 - List existing builds
 - Get information about a specific build
 - Get a detailed list of packages available in a specific build, that is
   re-usable into an mpb config file

The COPR clean command has been reworked to be more efficient.
New options have been added to packages:

 - Retry on failure: if a package failed to build, user can now request MPB to
   retry multiple times. A side effect of this feature is a reordering of the
   packages, which can be dumped and re-used for later builds, thus improving
   the success rate over time (hopefully)
 - Deps only: A package can be listed as to be used only to calculate reverse
   dependencies. That enables some use-cases like using pre-built packages
   (from koji) and yet rebuild their dependencies against them.
 - Globally skip arch: It is now possible to globally skip archs for reverse
   dependencies. That can be useful for packages which need to be built for
   both x86_64 and i686 but for which building reverse dependencies for both
   architectures makes no real sense or is to expensive in term of resources.
   See #17 for an example.

### Changes summary

- Add new options to package builds (Closes #17 and #30)
- UI improvements (Closes #13, #12, and #11)
- Rework clean command (Closes #38)

### Detailed changes

#### New Features

-  Print build info on demand @Frédéric Bérat [`dfe1d29`](https://gitlab.com/fedora/packager-tools/mass-prebuild/commit/dfe1d29c0afbd81625e7d4b8a923946a41842e54)
-  Add retry on package build failure @Frédéric Bérat [`753cf7f`](https://gitlab.com/fedora/packager-tools/mass-prebuild/commit/753cf7f06d6420d825591a5574f02007031e1ee6)
-  Get back-end from database @Frédéric Bérat [`334545d`](https://gitlab.com/fedora/packager-tools/mass-prebuild/commit/334545d2639b146b0d7cd7cadbbbaea6fd46fc66)
-  Allow to globally skip some archs for revdeps @Frédéric Bérat [`cc60e08`](https://gitlab.com/fedora/packager-tools/mass-prebuild/commit/cc60e08294b3c45cf8cfae9e421f27fcf5b15c60)
-  Add a deps_only option to packages @Frédéric Bérat [`144c28e`](https://gitlab.com/fedora/packager-tools/mass-prebuild/commit/144c28e31e9aa151cffb75cf6c561a93521bdc2a)
-  Add a capability to list builds @Frédéric Bérat [`d54ed15`](https://gitlab.com/fedora/packager-tools/mass-prebuild/commit/d54ed15c249298a94a4670fd7a9e916d6300aef4)
-  copr: Be more aggressive on clean @Frédéric Bérat [`df25851`](https://gitlab.com/fedora/packager-tools/mass-prebuild/commit/df25851b62118942e285ca2e9b16250bf3ee05ae)
-  Allow to restart from stage 0 @Frédéric Bérat [`c5e7533`](https://gitlab.com/fedora/packager-tools/mass-prebuild/commit/c5e75330693b40c047583cdd68d0f4bbd1d96ddd)

#### Fixes

-  Reconstruct config if not available in db @Frédéric Bérat [`fa6a4d6`](https://gitlab.com/fedora/packager-tools/mass-prebuild/commit/fa6a4d6a6afe5baba3b08e2a91484982dec93f70)
-  command line should overwrite config file @Frédéric Bérat [`255cc6c`](https://gitlab.com/fedora/packager-tools/mass-prebuild/commit/255cc6c6b23f38f7d586b4dff66adf808dd14ee5)
-  Enable checker build clean @Frédéric Bérat [`89540e6`](https://gitlab.com/fedora/packager-tools/mass-prebuild/commit/89540e65cb279479e957fa34b734e90914aa69f0)
-  Don't prepare checker build if not needed @Frédéric Bérat [`f20a254`](https://gitlab.com/fedora/packager-tools/mass-prebuild/commit/f20a25464269bf48b62779f0aa928fe7e0fb9ff8)
-  Reject malformed package entries @Frédéric Bérat [`0195055`](https://gitlab.com/fedora/packager-tools/mass-prebuild/commit/01950558c20f84eea4de55929be8fbdadccd4e95)
-  Checker to get data and dnf_conf field from parent build @Frédéric Bérat [`abea606`](https://gitlab.com/fedora/packager-tools/mass-prebuild/commit/abea60634ce79cacb758bd66913a8dae6d731557)

#### Documentation Changes

-  Describe the retry option @Frédéric Bérat [`7838793`](https://gitlab.com/fedora/packager-tools/mass-prebuild/commit/7838793833971c3bf37a2b6e444b4b7f29c5fe6b)
-  Add skip_archs global option @Frédéric Bérat [`347b172`](https://gitlab.com/fedora/packager-tools/mass-prebuild/commit/347b1726a65cdfc8a0b5ec71b13115df08f878b7)
-  Describe 'deps_only' package option @Frédéric Bérat [`9dc568a`](https://gitlab.com/fedora/packager-tools/mass-prebuild/commit/9dc568a15174d0bc40401aaa911ab23e7604f24c)

#### Refactoring and Updates

-  Move package creation argument to dict @Frédéric Bérat [`91f11d3`](https://gitlab.com/fedora/packager-tools/mass-prebuild/commit/91f11d3551b8ac0912c43ae8e331b7782172be14)
- (dummy): Improve log when verbosity is higher than 2 @Frédéric Bérat [`429d7b6`](https://gitlab.com/fedora/packager-tools/mass-prebuild/commit/429d7b690d4a0e9c3a6d457a27ff60f69d8d8a45)
-  Improve help for collect list @Frédéric Bérat [`0b2b8d3`](https://gitlab.com/fedora/packager-tools/mass-prebuild/commit/0b2b8d3402906d4953541947d6861c67d4debed6)
-  Rework package build status @Frédéric Bérat [`5cecde8`](https://gitlab.com/fedora/packager-tools/mass-prebuild/commit/5cecde8378a7e61cd4a3d256747178f7e16ab31d)

#### Changes to Test Assests

-  Add test for retry feature @Frédéric Bérat [`52d40ca`](https://gitlab.com/fedora/packager-tools/mass-prebuild/commit/52d40ca76f0fceb34c54afc3ccebf7288d5bcfa4)
-  Check that command line supersedes config file @Frédéric Bérat [`0f53ee5`](https://gitlab.com/fedora/packager-tools/mass-prebuild/commit/0f53ee56cc311450ccf8cf4e3c7b466a0d213bb7)
-  Add tests for build list and info @Frédéric Bérat [`14a97a6`](https://gitlab.com/fedora/packager-tools/mass-prebuild/commit/14a97a6d70e1382b4a080437a65486e9ebe3e11c)
-  Check the 'deps_only' option @Frédéric Bérat [`794cd57`](https://gitlab.com/fedora/packager-tools/mass-prebuild/commit/794cd57c5533bd6f63c5d272b191af1c20dd6f96)
-  Re-use back-end info from database @Frédéric Bérat [`844e526`](https://gitlab.com/fedora/packager-tools/mass-prebuild/commit/844e526979e0bfe69edf1ef6277f80c4f1d532b9)

#### Tidying of Code eg Whitespace

-  bring consistency in naming {with,after}_* @Frédéric Bérat [`7929807`](https://gitlab.com/fedora/packager-tools/mass-prebuild/commit/7929807b0d2ecce61b53b7a6fc8a3d69582f3f57)

#### General Changes

- ci: Allow execution of a subset of the regression tests @Frédéric Bérat [`e3cb48c`](https://gitlab.com/fedora/packager-tools/mass-prebuild/commit/e3cb48cbebfa2e3b83c947c8a885d65baa4abb03)
- Various fixes @Frédéric Bérat [`69e18ce`](https://gitlab.com/fedora/packager-tools/mass-prebuild/commit/69e18ce924336f9d4fa0df049d275adcd7a46ea7)

## [v0.3.0](https://gitlab.com/fberat/mass-prebuild/-/releases/v0.3.0) - 13 October 2022

### Release Highlights

This release mainly fixes issues reported by new users.

The reverse dependency calculation better support virtual packages, and now
include runtime dependencies.

Support for non-public COPR and Koji instances is working (again), although
there is a need for a user specific repositories configuration. The way to
provide these configurations will be improved in later releases.

The database had to be reworked in a non-forward compatible way, once it has
been migrated to the new schema, the older version of mass-prebuilder won´t
work with the new database. Yet, the database is saved before this breaking
change, so that it is possible to rewind, at least partially.

### Changes summary

- Rework reverse dependency calculation (Closes #20 and #43)
- Fix broken centos-stream support (Closes #41)
- Various fixes (Closes #42)

### Detailed changes

#### New Features

-  Add support for group of packages @Frédéric Bérat [`49e76e3`](https://gitlab.com/fedora/packager-tools/mass-prebuild/commit/49e76e3da022d353540937e02c878f0deb68ee27)
-  whatrequires: Add support for arch specific repositories @Frédéric Bérat [`2865b30`](https://gitlab.com/fedora/packager-tools/mass-prebuild/commit/2865b307038e3a742975708af49a376c1d483822)
-  Add rhel support for gitnbranch @Frédéric Bérat [`c19c673`](https://gitlab.com/fedora/packager-tools/mass-prebuild/commit/c19c67316906ad318366cf18b63200e3331762e8)

#### Fixes

-  Properly append packages @Frédéric Bérat [`f33b8ef`](https://gitlab.com/fedora/packager-tools/mass-prebuild/commit/f33b8ef0a9b06be0e6c13b9994e3f816c00a3e80)
-  copr: Add default values in the arch table @Frédéric Bérat [`7ce02e8`](https://gitlab.com/fedora/packager-tools/mass-prebuild/commit/7ce02e8c15025cd85a9012347c381be7bcf72ff4)
-  Correct repo info for centos-stream @Frédéric Bérat [`8b5d48b`](https://gitlab.com/fedora/packager-tools/mass-prebuild/commit/8b5d48b73094c412224fc2fb21aa6db0a362eccd)
-  Use default branch name if commit ID can't be found in Koji @Frédéric Bérat [`7d15841`](https://gitlab.com/fedora/packager-tools/mass-prebuild/commit/7d15841dcfb82f93741b44ed8f65f77bf69700d3)
-  whatrequires: Support stdout output from within a container @Frédéric Bérat [`f433a4d`](https://gitlab.com/fedora/packager-tools/mass-prebuild/commit/f433a4da1d68b95e0ab89c1ff7401699dec3f056)
-  Raise an error if an invalid build ID is provided @Frédéric Bérat [`de27c8e`](https://gitlab.com/fedora/packager-tools/mass-prebuild/commit/de27c8e9959886b253b726d529c19fa215ee3dc5)
-  Handle empty package list case @Frédéric Bérat [`bee5e95`](https://gitlab.com/fedora/packager-tools/mass-prebuild/commit/bee5e954cfbe56788b97b0ba61261f9d310e653d)
-  Adapt dummy backend to collect_data new prototype @Frédéric Bérat [`6029005`](https://gitlab.com/fedora/packager-tools/mass-prebuild/commit/6029005e5efe1ea23b40a6068150d6c6a1d0dd5b)
-  non-zero exit in case of errors @Frédéric Bérat [`c5afa09`](https://gitlab.com/fedora/packager-tools/mass-prebuild/commit/c5afa094f507558f96dacaf2cff096b4924080e7)
-  Fix package list depth @Frédéric Bérat [`12ac42b`](https://gitlab.com/fedora/packager-tools/mass-prebuild/commit/12ac42bab997a6e088f630fe0b6d0d2843bd6e51)
-  Correct Centos-stream branch names @Frédéric Bérat [`309c14f`](https://gitlab.com/fedora/packager-tools/mass-prebuild/commit/309c14ff462e1c94ba837086209f314bed3b5d45)
-  Init 'validity' config field @Frédéric Bérat [`9534192`](https://gitlab.com/fedora/packager-tools/mass-prebuild/commit/9534192964ca6f89110d6d5d5cea28c83b02e181)

#### Documentation Changes

-  Refill changelog @Frédéric Bérat [`d52a6c7`](https://gitlab.com/fedora/packager-tools/mass-prebuild/commit/d52a6c7c727478050732db6ab3053b527bc0e09b)
-  Update centos-stream example config @Frédéric Bérat [`8a05e48`](https://gitlab.com/fedora/packager-tools/mass-prebuild/commit/8a05e481e68097756d72a45b6731a22f4afcd039)
-  Add support for sub-packages and groups in package config @Frédéric Bérat [`1eb8752`](https://gitlab.com/fedora/packager-tools/mass-prebuild/commit/1eb875208b200bf57702bc9b1b7fb63675568460)

#### Refactoring and Updates

- (whatrequires): Rework reverse deps calculation @Frédéric Bérat [`88b1351`](https://gitlab.com/fedora/packager-tools/mass-prebuild/commit/88b1351f34f966596a02fb0e1e7e09cd08cc4367)
- (db): Introduce DB versions @Frédéric Bérat [`831b76b`](https://gitlab.com/fedora/packager-tools/mass-prebuild/commit/831b76be825d67b1c94de13a93c71cc9fb0179aa)
- (backend): Reorganize attribute initialization @Frédéric Bérat [`6fe2c05`](https://gitlab.com/fedora/packager-tools/mass-prebuild/commit/6fe2c05ba3ac64e5411fbce0ae0f9664e831ed0c)
-  Allow linear logging when verbosity is higher than 4 @Frédéric Bérat [`ac8082c`](https://gitlab.com/fedora/packager-tools/mass-prebuild/commit/ac8082cf55d0dc526877f570e2b73beeebac300c)
- (whatrequires): Rework get_last_build @Frédéric Bérat [`02be504`](https://gitlab.com/fedora/packager-tools/mass-prebuild/commit/02be50436b2df00cfaabfd2a3706008e349065db)

#### Changes to Test Assests

-  Test package group support @Frédéric Bérat [`a8d7af1`](https://gitlab.com/fedora/packager-tools/mass-prebuild/commit/a8d7af1e93b3518a141cc4e6594cfd8e79ec8ba0)
-  Add tests for reverse dependency @Frédéric Bérat [`77b98de`](https://gitlab.com/fedora/packager-tools/mass-prebuild/commit/77b98def06dd259a72a5fa6098b250dcaa66c1a2)
-  Decrease dummy backend failure rate @Frédéric Bérat [`bea680c`](https://gitlab.com/fedora/packager-tools/mass-prebuild/commit/bea680c56bce924eeeeb72268711ee4ac3274fd2)

#### Tidying of Code eg Whitespace

-  Improve log to list packages being built @Frédéric Bérat [`60b8e52`](https://gitlab.com/fedora/packager-tools/mass-prebuild/commit/60b8e5241f5450e6d359c8ac42d7faecb0a85aee)
-  Fix typo in mpb.config.yaml @Frédéric Bérat [`7a8ab38`](https://gitlab.com/fedora/packager-tools/mass-prebuild/commit/7a8ab38eef93ad0c5a571b6399bc7ea159966786)
-  Fix typo in copr test @Frédéric Bérat [`e077e18`](https://gitlab.com/fedora/packager-tools/mass-prebuild/commit/e077e186313cd4112b91451f9ce545bf7ee91b9f)

#### General Changes

- ci: Add support for local execution of CI @Frédéric Bérat [`08984e9`](https://gitlab.com/fedora/packager-tools/mass-prebuild/commit/08984e96f62513d7866067de41d563154506d1dc)
- Improve support for non-public infrastructures () @Frédéric Bérat [`0cda583`](https://gitlab.com/fedora/packager-tools/mass-prebuild/commit/0cda583ca4f8bc70b0555812b202e45be7357ddb)
- ci: Add support for local execution of CI () @Frédéric Bérat [`1bfca61`](https://gitlab.com/fedora/packager-tools/mass-prebuild/commit/1bfca61c14db0128af52b2199636cce63c485a1a)

## [v0.2.0](https://gitlab.com/fberat/mass-prebuild/-/releases/v0.2.0) - 3 September 2022

### Release Highlights

This release brings the tool closer to version 1.0.0, as it brings
some features that were planned in the design document.

Data collection is now fully implemented.

Users can provide custom configurations for DNF. That allows them to
restrict the reverse dependencies calculation through repositories,
e.g.: one may provide ELN repositories for a Rawhide build.

Improvement in the handling of the COPR back-end leads to lower stress on
the infrastructure, and better support for unstable connections.

Project README file is reworked, and the example configuration file
provide extensive description of the available options.

See [Release v0.2.0](https://gitlab.com/fberat/mass-prebuild/-/merge_requests/23) merge request.

### Changes summary

-  Fully implement data collection (Closes #37, #32, and #15)
-  Update readme and config example (Closes #35)
-  Rework COPR function calls (Closes #27 and #29)
-  Opt-out from resolving priorities (Closes #28)
-  Skip UNCONFIRMED failures in watch worker (Closes #31)
-  Reject invalid package names (Closes #33)

### Detailed changes

#### New Features

-  Allow users to specify a dnf repo for querying source rpms @Frédéric Bérat [`05eb125`](https://gitlab.com/fberat/mass-prebuild/commit/05eb12548b2dc4a519c7aeca5bb5906091c18f20)
-  Opt-out from resolving priorities @Tomáš Hrnčiar [`ec3f2e4`](https://gitlab.com/fberat/mass-prebuild/commit/ec3f2e4229efdc0da06b7072ae292c8ba3665ae7)
-  Fully implement collect operation @Frédéric Bérat [`5c5d04b`](https://gitlab.com/fberat/mass-prebuild/commit/5c5d04b8c47df4a46873c84f3ad1e2fc00d67a7a)
-  Separate collected data per build status @Frédéric Bérat [`3a2fd5f`](https://gitlab.com/fberat/mass-prebuild/commit/3a2fd5f3a4829447c8c106e50921859977198758)
-  Allow users to specify a dnf repo for querying source rpms () @fberat [`6d54c28`](https://gitlab.com/fberat/mass-prebuild/commit/6d54c28964166b726ee1b38ef026554f208cb96b)

#### Fixes

-  Rework COPR build monitoring @Frédéric Bérat [`4511ab9`](https://gitlab.com/fberat/mass-prebuild/commit/4511ab901854b2c3b0627652bb0d0550d9238c3a)
-  Retry COPR function on connection failure @Frédéric Bérat [`dd62d8b`](https://gitlab.com/fberat/mass-prebuild/commit/dd62d8b5b8a50c04f803c16c9ae3e2da56803ce7)
-  Reject invalid package names @Frédéric Bérat [`baa0c45`](https://gitlab.com/fberat/mass-prebuild/commit/baa0c45e01d0e9c769ebd7169cbd9fe43293992a)
-  Config validity @Frédéric Bérat [`4e88907`](https://gitlab.com/fberat/mass-prebuild/commit/4e889077bd891e1209604cbf32da3622dc672f76)
-  Collect list typo @Frédéric Bérat [`ef19e7a`](https://gitlab.com/fberat/mass-prebuild/commit/ef19e7ad4c8286aa4076f8bf603d50d490aa5473)
-  Remove shebang @Frédéric Bérat [`9096443`](https://gitlab.com/fberat/mass-prebuild/commit/909644382d1a842a354d010241a7a257c88817c7)
-  backend: Skip UNCONFIRMED failures in watch worker @Frederic Berat [`4a3d600`](https://gitlab.com/fberat/mass-prebuild/commit/4a3d6001bb5f8c4c157192f615726962f1fac58f)

#### Documentation Changes

-  README: Give a proper presentation of the tool @Frédéric Bérat [`6351bc4`](https://gitlab.com/fberat/mass-prebuild/commit/6351bc4ca7f6748995366607b61ce6dc134e2bb3)
-  Add support for semi-automatic changelog @Frederic Berat [`285b544`](https://gitlab.com/fberat/mass-prebuild/commit/285b5448ebc36eddd65fa54743cc834acb107116)
-  Detail configuration file @Frédéric Bérat [`8e9cf7e`](https://gitlab.com/fberat/mass-prebuild/commit/8e9cf7ec17b35a6bbe2182687e68f6ba9ce72d2a)
-  Update CHANGELOG with release details @Frédéric Bérat [`e9b792c`](https://gitlab.com/fberat/mass-prebuild/commit/e9b792c07d49a5e157930f76a971937c550a5289)
-  Add support for semi-automatic changelog () @fberat [`1600b20`](https://gitlab.com/fberat/mass-prebuild/commit/1600b2006421e30822be0332934d7e707b16e5d9)

#### Refactoring and Updates

-  Rework COPR builder functions @Frédéric Bérat [`f9c35b8`](https://gitlab.com/fberat/mass-prebuild/commit/f9c35b89b41447521f8810cc1bacc5b52302a0e8)
-  Review config handling @Frédéric Bérat [`d15be24`](https://gitlab.com/fberat/mass-prebuild/commit/d15be24cfc25cad1d7299494f5e162fdb3e91131)

#### General Changes

- ci: Add commit lint check @Frederic Berat [`da564b1`](https://gitlab.com/fberat/mass-prebuild/commit/da564b1985d2b19dd994eb01bead51874460430d)
- ci: Add markdown linter @Frédéric Bérat [`249120a`](https://gitlab.com/fberat/mass-prebuild/commit/249120a12d3fde6231ae87bb3f11359694c6af10)
- ci: Add "release" to commit lint rules @Frédéric Bérat [`e2e54cc`](https://gitlab.com/fberat/mass-prebuild/commit/e2e54cc44970daab2e5be1634daad5762427cdb1)
- ci: Prevent commit lint when not in merge request context @Frédéric Bérat [`5ed72c4`](https://gitlab.com/fberat/mass-prebuild/commit/5ed72c4ac02ebbd27c1a9e30c16232f41a307b9f)
- ci: Update black @Frédéric Bérat [`ac68951`](https://gitlab.com/fberat/mass-prebuild/commit/ac68951348ab3a74b6752e621b3e4b6bfd3e80b5)
- ci: Allow pipeline to be executed out of merge requests @Frédéric Bérat [`c08100c`](https://gitlab.com/fberat/mass-prebuild/commit/c08100cd3b0b2ae677b3db05a20fa1004ca707a3)

## [v0.1.1](https://gitlab.com/fberat/mass-prebuild/-/releases/v0.1.1) - 24 August 2022

### Release Highlights

Moving out of the "alpha" naming, although there is still heavy development
ongoing. All the bugs spotted on alpha 1 are still not fixed, but there is
improvement already.

Starting from this release, Gitlab CI is enabled and will be improved over
time.

### Features

- Enable Gitlab CI [`!13`](https://gitlab.com/fberat/mass-prebuild/merge_requests/13)
- Build revdeps from a known state [`!10`](https://gitlab.com/fberat/mass-prebuild/merge_requests/10)
- backend:copr: Various configuration improvement [`!12`](https://gitlab.com/fberat/mass-prebuild/merge_requests/12)
- Log unhandled exceptions into a file [`!9`](https://gitlab.com/fberat/mass-prebuild/merge_requests/9)

### Fixes

- Fix support for x86 and 'all' archs [`!11`](https://gitlab.com/fberat/mass-prebuild/merge_requests/11)
- Close cursor after each transactions [`!7`](https://gitlab.com/fberat/mass-prebuild/merge_requests/7)
- whatrequires: improve support for libraries and Rawhide [`!8`](https://gitlab.com/fberat/mass-prebuild/merge_requests/8)
- Fix support for x86 and 'all' arches [`50fb478`](https://gitlab.com/fberat/mass-prebuild/commit/50fb4787c22ade95ba009f8a96545f1c09ebf545)
- backend: db: Close cursor after each transactions [`78f7cfb`](https://gitlab.com/fberat/mass-prebuild/commit/78f7cfb96b8525129613a7286e75a80c452388f8)

## [v0.1.0-alpha1](https://gitlab.com/fberat/mass-prebuild/-/releases/v0.1.0-alpha1) - 5 July 2022

### Release Highlights

This is the first public release of the mass-prebuild tool.
Major changes from pre-alpha:
 - Architecture has been re-assessed
 - Backend implementation has been re-written

### Features

- Alpha-1: Implement new back-end processing [`!4`](https://gitlab.com/fberat/mass-prebuild/merge_requests/4)
- backend: New implementation [`70ed45a`](https://gitlab.com/fberat/mass-prebuild/commit/70ed45a68ca1e57ff1b7df1d1915231c72eaea1d)
- backend:copr: Adapt to new backend implementation [`b3e8a03`](https://gitlab.com/fberat/mass-prebuild/commit/b3e8a0301f4e8fa9559ba9113af26d080a755efd)
- backend:db: Implement new database [`822970a`](https://gitlab.com/fberat/mass-prebuild/commit/822970ad93e91c56ace1b97eefe94bc3847c3318)

### Fixes

- Bump default timeout to 30 hours [`!3`](https://gitlab.com/fberat/mass-prebuild/merge_requests/3)

### Doc

- Doc: Redesign [`!2`](https://gitlab.com/fberat/mass-prebuild/merge_requests/2)

### General Changes
- Add spec file for package build [`0b12856`](https://gitlab.com/fberat/mass-prebuild/commit/0b128568056632d1859c9e8914c68982b148c2d6)
- packaging: prepare packaging for alpha 1 [`4548b4d`](https://gitlab.com/fberat/mass-prebuild/commit/4548b4d97a096b943ac2db0810c29aa38e1e4c9e)
